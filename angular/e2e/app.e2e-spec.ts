import { FeatureCheckerTestTemplatePage } from './app.po';

describe('FeatureCheckerTest App', function() {
  let page: FeatureCheckerTestTemplatePage;

  beforeEach(() => {
    page = new FeatureCheckerTestTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
