﻿namespace FeatureCheckerTest
{
    public class FeatureCheckerTestConsts
    {
        public const string LocalizationSourceName = "FeatureCheckerTest";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
