﻿using Abp.Authorization;
using FeatureCheckerTest.Authorization.Roles;
using FeatureCheckerTest.Authorization.Users;

namespace FeatureCheckerTest.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
