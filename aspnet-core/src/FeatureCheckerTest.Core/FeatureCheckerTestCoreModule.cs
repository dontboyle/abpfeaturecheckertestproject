﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Timing;
using Abp.Zero;
using Abp.Zero.Configuration;
using FeatureCheckerTest.Authorization.Roles;
using FeatureCheckerTest.Authorization.Users;
using FeatureCheckerTest.Configuration;
using FeatureCheckerTest.Features;
using FeatureCheckerTest.Localization;
using FeatureCheckerTest.MultiTenancy;
using FeatureCheckerTest.Timing;

namespace FeatureCheckerTest
{
    [DependsOn(typeof(AbpZeroCoreModule))]
    public class FeatureCheckerTestCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            // Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            FeatureCheckerTestLocalizationConfigurer.Configure(Configuration.Localization);

            //Adding feature providers
            Configuration.Features.Providers.Add<AppFeatureProvider>();

            // Enable this line to create a multi-tenant application.
            Configuration.MultiTenancy.IsEnabled = FeatureCheckerTestConsts.MultiTenancyEnabled;

            // Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);

            Configuration.Settings.Providers.Add<AppSettingProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FeatureCheckerTestCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
        }
    }
}
