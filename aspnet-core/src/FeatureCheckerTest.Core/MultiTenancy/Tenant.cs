﻿using Abp.MultiTenancy;
using FeatureCheckerTest.Authorization.Users;

namespace FeatureCheckerTest.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
