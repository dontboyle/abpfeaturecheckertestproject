﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FeatureCheckerTest.Features
{
    public static class AppFeatures
    {
        public const string testFeature = "App.TestFeature";
        public const string tylerFeature = "App.TestFeature.Tyler";
        public const string pattonFeature = "App.TestFeature.Patton";
    }
}
