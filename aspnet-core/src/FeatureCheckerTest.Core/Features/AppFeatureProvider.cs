﻿using Abp.Application.Features;
using Abp.UI.Inputs;
using Abp.Localization;
using System;
using System.Collections.Generic;
using System.Text;

namespace FeatureCheckerTest.Features
{
    public class AppFeatureProvider : FeatureProvider
    {
        public override void SetFeatures(IFeatureDefinitionContext context)
        {
            var testFeature = context.Create(
                AppFeatures.testFeature,
                defaultValue: "false",
                displayName: L("TestFeature"),
                inputType: new CheckboxInputType()
            );
            testFeature.CreateChildFeature(
                AppFeatures.tylerFeature,
                defaultValue: "false",
                displayName: L("TylerFeature"),
                inputType: new CheckboxInputType()
            );
            testFeature.CreateChildFeature(
                AppFeatures.pattonFeature,
                defaultValue: "false",
                displayName: L("PattonFeature"),
                inputType: new CheckboxInputType()
            );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, "SourceFiles");
        }
    }
}   
