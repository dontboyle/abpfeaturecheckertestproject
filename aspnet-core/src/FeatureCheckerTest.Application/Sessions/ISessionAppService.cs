﻿using System.Threading.Tasks;
using Abp.Application.Services;
using FeatureCheckerTest.Sessions.Dto;

namespace FeatureCheckerTest.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
