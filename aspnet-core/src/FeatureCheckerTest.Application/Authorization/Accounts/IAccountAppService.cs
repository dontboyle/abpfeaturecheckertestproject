﻿using System.Threading.Tasks;
using Abp.Application.Services;
using FeatureCheckerTest.Authorization.Accounts.Dto;

namespace FeatureCheckerTest.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
