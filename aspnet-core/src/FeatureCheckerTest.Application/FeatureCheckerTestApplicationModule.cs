﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using FeatureCheckerTest.Authorization;

namespace FeatureCheckerTest
{
    [DependsOn(
        typeof(FeatureCheckerTestCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class FeatureCheckerTestApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<FeatureCheckerTestAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(FeatureCheckerTestApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
