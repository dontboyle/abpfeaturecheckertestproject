using System.ComponentModel.DataAnnotations;

namespace FeatureCheckerTest.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}