﻿using System.Threading.Tasks;
using FeatureCheckerTest.Configuration.Dto;

namespace FeatureCheckerTest.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
