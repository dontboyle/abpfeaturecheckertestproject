﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using FeatureCheckerTest.Configuration.Dto;

namespace FeatureCheckerTest.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : FeatureCheckerTestAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
