﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using FeatureCheckerTest.MultiTenancy.Dto;

namespace FeatureCheckerTest.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

