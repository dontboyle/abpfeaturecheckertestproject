﻿using Abp;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using FeatureCheckerTest.Authorization.Roles;
using FeatureCheckerTest.Authorization.Users;
using FeatureCheckerTest.Editions;
using FeatureCheckerTest.Features;
using FeatureCheckerTest.MultiTenancy.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeatureCheckerTest.MultiTenancy
{
    public class TenantFeatureManagement : TenantAppService, ITenantFeatureManagement
    {
        private readonly TenantManager _tenantManager;
        private readonly EditionManager _editionManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;
        private readonly IFeatureChecker _featureChecker;

        public TenantFeatureManagement(
            IRepository<Tenant, int> repository,
            TenantManager tenantManager,
            EditionManager editionManager,
            UserManager userManager,
            RoleManager roleManager,
            IAbpZeroDbMigrator abpZeroDbMigrator,
            IFeatureChecker featureChecker)
            : base(repository, 
                  tenantManager, 
                  editionManager, 
                  userManager, roleManager, 
                  abpZeroDbMigrator)
        {
            _tenantManager = tenantManager;
            _featureChecker = featureChecker;
        }
        public async Task<List<string>> CreateTenant(CreateTenantDto input)
        {
            var tenant = await Create(input);
            return await CheckingTheFeatures(tenant.Id);
        }

        public async Task<List<string>> ChangeFeatures(int tenantId, List<NameValueDto> featureValues)
        {
            await _tenantManager.SetFeatureValuesAsync(tenantId,
                featureValues.Select(fv => new NameValue(fv.Name, fv.Value)).ToArray());

            return await CheckingTheFeatures(tenantId);
        }

        public async Task<List<string>> DeleteFeature(int tenantId)
        {
            await _tenantManager.ResetAllFeaturesAsync(tenantId);

            return await CheckingTheFeatures(tenantId);
        }

        private async Task<List<string>> CheckingTheFeatures(int tenantId)
        {
            List<string> featureList = new List<string>();
            if (await _featureChecker.IsEnabledAsync(tenantId, AppFeatures.tylerFeature))
            {
                featureList.Add("tyler feature");
            }
            if (await _featureChecker.IsEnabledAsync(tenantId, AppFeatures.pattonFeature))
            {
                featureList.Add("patton feature");
            }
            featureList.Add("End of list");
            return featureList;
        }
    }
}
