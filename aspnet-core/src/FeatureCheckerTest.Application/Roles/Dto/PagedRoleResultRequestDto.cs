﻿using Abp.Application.Services.Dto;

namespace FeatureCheckerTest.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

