﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using FeatureCheckerTest.Configuration;
using FeatureCheckerTest.Web;

namespace FeatureCheckerTest.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class FeatureCheckerTestDbContextFactory : IDesignTimeDbContextFactory<FeatureCheckerTestDbContext>
    {
        public FeatureCheckerTestDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<FeatureCheckerTestDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            FeatureCheckerTestDbContextConfigurer.Configure(builder, configuration.GetConnectionString(FeatureCheckerTestConsts.ConnectionStringName));

            return new FeatureCheckerTestDbContext(builder.Options);
        }
    }
}
