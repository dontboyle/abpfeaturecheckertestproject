using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace FeatureCheckerTest.EntityFrameworkCore
{
    public static class FeatureCheckerTestDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<FeatureCheckerTestDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<FeatureCheckerTestDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
