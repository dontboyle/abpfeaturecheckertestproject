﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using FeatureCheckerTest.Authorization.Roles;
using FeatureCheckerTest.Authorization.Users;
using FeatureCheckerTest.MultiTenancy;

namespace FeatureCheckerTest.EntityFrameworkCore
{
    public class FeatureCheckerTestDbContext : AbpZeroDbContext<Tenant, Role, User, FeatureCheckerTestDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public FeatureCheckerTestDbContext(DbContextOptions<FeatureCheckerTestDbContext> options)
            : base(options)
        {
        }
    }
}
