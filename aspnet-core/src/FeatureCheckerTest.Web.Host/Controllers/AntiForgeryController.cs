using Microsoft.AspNetCore.Antiforgery;
using FeatureCheckerTest.Controllers;

namespace FeatureCheckerTest.Web.Host.Controllers
{
    public class AntiForgeryController : FeatureCheckerTestControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
