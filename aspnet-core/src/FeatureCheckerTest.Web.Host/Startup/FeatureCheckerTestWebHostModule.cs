﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using FeatureCheckerTest.Configuration;

namespace FeatureCheckerTest.Web.Host.Startup
{
    [DependsOn(
       typeof(FeatureCheckerTestWebCoreModule))]
    public class FeatureCheckerTestWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public FeatureCheckerTestWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FeatureCheckerTestWebHostModule).GetAssembly());
        }
    }
}
