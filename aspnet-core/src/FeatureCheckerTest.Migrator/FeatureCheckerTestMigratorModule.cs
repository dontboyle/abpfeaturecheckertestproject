using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using FeatureCheckerTest.Configuration;
using FeatureCheckerTest.EntityFrameworkCore;
using FeatureCheckerTest.Migrator.DependencyInjection;

namespace FeatureCheckerTest.Migrator
{
    [DependsOn(typeof(FeatureCheckerTestEntityFrameworkModule))]
    public class FeatureCheckerTestMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public FeatureCheckerTestMigratorModule(FeatureCheckerTestEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(FeatureCheckerTestMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                FeatureCheckerTestConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(FeatureCheckerTestMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
