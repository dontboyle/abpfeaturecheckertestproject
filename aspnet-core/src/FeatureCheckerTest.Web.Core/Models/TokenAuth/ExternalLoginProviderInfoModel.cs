﻿using Abp.AutoMapper;
using FeatureCheckerTest.Authentication.External;

namespace FeatureCheckerTest.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
