using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace FeatureCheckerTest.Controllers
{
    public abstract class FeatureCheckerTestControllerBase: AbpController
    {
        protected FeatureCheckerTestControllerBase()
        {
            LocalizationSourceName = FeatureCheckerTestConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
