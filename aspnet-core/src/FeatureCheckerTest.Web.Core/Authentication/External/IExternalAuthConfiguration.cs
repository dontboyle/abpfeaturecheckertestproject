﻿using System.Collections.Generic;

namespace FeatureCheckerTest.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
